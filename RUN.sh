# In case of problems
# chmod +x RUN.sh

# Enable exclude and globstar
shopt -s extglob
shopt -s globstar

# Compile everything
# javac **/*.java
# Or: javac $(find . -name "*.java")
mkdir -p build
javac -Xlint:unchecked -d ./build  src/!(tests)/**/*.java # except tests, with displaying warnings

# Move to the build folder
cd ./build || exit

# Build 'NN.jar' with everything inside | Executable: main.Mnist
jar cfe ../NN.jar main.Mnist ./**/*.class
cd .. # move back
rm -r ./build # purge everything

# Execute the main class
java -jar ./NN.jar "30" # + arguments

# Display results
echo -e "\n"
java -jar "pv021_project/fi-muni-pv021-automatic-evaluator-1.0-SNAPSHOT-jar-with-dependencies.jar" "./actualTestPredictions" "MNIST_DATA/mnist_test_labels.csv" 10
cat Results
echo -e "\n"