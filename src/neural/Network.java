package neural;

import mathematics.MathUtils;
import mathematics.Matrix;
import neural.functions.ActivationFunction;
import utils.Pair;

import java.util.*;

/**
 * The entire neural network.
 * Divided in {@link Layer layers}.
 * @see Layer
 */
public class Network {

    /** All layers of the network, sequentially. */
    private final List<Layer> layers = new ArrayList<>();
    /** (Input, Output) dimensions of the neural network. */
    private final Pair<Integer> dimensions;
    /** Currently processed minibatch of the network. */
    private final Minibatch minibatch;

    /** A hyperparameter that controls how much to change the model in response to the estimated error each time the model weights are updated.
     * <a href="https://machinelearningmastery.com/understand-the-dynamics-of-learning-rate-on-deep-learning-neural-networks/">
     * Learning Rate and Gradient Descent</a> */
    private double learningRate;

    /**
     * Creates a network with given parameters.
     * @param activationFunction An activation function of the network.
     * @param learningRate The learning rate of the network.
     * @param minibatchSize The size of a {@link Minibatch minibatch}.
     * @param layers The topology of the network (from a, through hidden, to b layers).
     */
    public Network(ActivationFunction activationFunction, double learningRate, int minibatchSize, int... layers) {
        setLearningRate(learningRate);

        // input, output size
        dimensions = new Pair<>();
        dimensions.a = layers[0];
        dimensions.b = layers[layers.length - 1];

        // make a minibatch
        minibatch = new Minibatch(minibatchSize);

        // add all hidden layers
        for (int i = 1; i < layers.length - 1; i++) {
            int layerSize = layers[i];
            Pair<Integer> currentDimensions = new Pair<>(layers[i-1], layers[i+1]);
            Layer layer = new Layer(layerSize, activationFunction, currentDimensions);
            this.layers.add(layer);
        }

        // final output layer
        this.layers.add(new Layer(dimensions.b, activationFunction,
                new Pair<>(layers[layers.length - 2], dimensions.b)));

    }

    // Getters and setters --------------------------------------------

    public List<Layer> getLayers() { return layers;    }
    public Pair<Integer> getDimensions() { return dimensions; }
    public Minibatch getMinibatch() { return minibatch; }
    public void setLearningRate(double learningRate) { this.learningRate = MathUtils.clamp01(learningRate); }

    // ----------------------------------------------------------------

    /** Return the output of the network for a given input vector. */
    public void feedForward() {
        // TODO: https://youtu.be/aircAruvnKk?list=PLZHQObOWTQDNU6R1_67000Dx_ZCJB-3pi&t=916
        Matrix minibatchData = getMinibatch().getInputs();
        for (Layer layer : layers) {
            minibatchData = layer.getWeights().multiply(minibatchData);

            // Add all biases
            int cols = minibatchData.getRow(0).length;
            int rows = minibatchData.getRowCount();
            for (int col = 0; col < cols; col++) {
                for (int row = 0; row < rows; row++) {
                    double value = minibatchData.getElement(row, col);
                    minibatchData.setElement(row, col, value + layer.getBiases()[row]);
                }
            }

            // Set the potentials | ! NEW MATRIX ! OTHERWISE ALL VALUES OVERWRITE EACH OTHER !
            layer.setPotentials(new Matrix(minibatchData));
            // Apply the activation function
            ActivationFunction f = layer.getActivationFunction();
            minibatchData = minibatchData.map(f::function);
            // Set outputs
            layer.setOutputs(new Matrix(minibatchData)); // TODO: Check | Doesn't it change potentials?
        }
    }

    /** Algorithm determining how a single training example would like to nudge the weights and biases.
     * What relative proportions cause the most rapid decrease to the cost? */
    public void backPropagation() {
        // TODO: https://youtu.be/Ilg3gGewQ5U
        Layer lastLayer = layers.get(layers.size() - 1);
        ActivationFunction dF = lastLayer.getActivationFunction();

        // Output layer
        lastLayer.setPotentials(lastLayer.getPotentials().map(dF::functionDerivative)); // apply af derivative
        Matrix sMatrix = lastLayer.getOutputs().subtract(minibatch.getLabels());
        lastLayer.setGammas(new Matrix(sMatrix.product(lastLayer.getPotentials())));

        // Go from the output layer backwards
        for (int i = layers.size() - 2; i >= 0; i--) {
            Layer layer = layers.get(i);
            Layer previousLayer = layers.get(i+1);
            layer.setPotentials(layer.getPotentials().map(dF::functionDerivative)); // apply af derivative
            Matrix mMatrix = previousLayer.getWeights().transpose().multiply(previousLayer.getGammas());
            layer.setGammas(new Matrix(mMatrix.product(layer.getPotentials())));
        }
    }

    /** Stochastic gradient descent learning. */
    public void gradientDescent() {
        // TODO: https://youtu.be/IHZwWFHWa-w
        double learning = learningRate / minibatch.getSize();

        // First layer
        Layer firstLayer = layers.get(0);
        Matrix W = firstLayer.getWeights().subtract(
                firstLayer.getGammas()
                        .multiply(minibatch.getInputs().transpose())
                        .multiply(learning)
        );
        firstLayer.setWeights(W);
        double[] biases = firstLayer.getBiases();
        double[] vector = firstLayer.getGammas().multiply(learning).addUpRows();
        MathUtils.subtractAll(biases, vector);
        firstLayer.setBiases(biases);

        // Remaining layers
        for (int i = 1; i < layers.size(); i++) {
            Layer layer = layers.get(i);
            Layer previousLayer = layers.get(i-1);
            Matrix Wi = layer.getWeights().subtract(
                layer.getGammas()
                        .multiply(previousLayer.getOutputs().transpose())
                        .multiply(learning)
            );
            layer.setWeights(Wi);
            double[] biases_i = layer.getBiases();
            double[] vector_i = layer.getGammas().multiply(learning).addUpRows();
            MathUtils.subtractAll(biases_i, vector_i);
            layer.setBiases(biases_i);
        }
    }

    /** Updates the {@link Minibatch#getPredictions()} predictions based on the current outputs. */
    public void predict() {
        List<Integer> predictions = new ArrayList<>();
        Matrix outputs = layers.get(layers.size() - 1).getOutputs().transpose();
        for (double[] row : outputs.getData()) {
            int prediction = MathUtils.maxIndex(row);
            predictions.add(prediction);
        }
        minibatch.setPredictions(predictions);
    }

    // TODO: Heuristics >> weight decay, momentum, dropout etc.
}
