package neural;

import mathematics.Functions;
import mathematics.MathUtils;
import mathematics.Matrix;
import neural.functions.ActivationFunction;
import utils.Pair;

import java.util.ArrayList;

/**
 * A layer inside a neural network.
 *
 * @see LayerType
 */
public class Layer {

    private final ActivationFunction activationFunction;

    private Matrix weights; // neuron weights
    private double[] biases; // neuron biases
    private Matrix potentials; // neuron values
    private Matrix outputs; // layer outputs
    private Matrix gammas; // gradient step

    // Count
    private final int neuronCount;
    /** Layer dimensions {@code (input,output)}. */
    final Pair<Integer> dimensions;

    public Layer(int neurons, ActivationFunction activationFunction, Pair<Integer> dimensions) {
        this.neuronCount = neurons;
        this.dimensions = dimensions;
        this.activationFunction = activationFunction;
        prepare();
    }

    // Getters and setters --------------------------------------------

    public Matrix getWeights() { return weights; }
    public void setWeights(Matrix weights) { this.weights = weights; }

    public double[] getBiases() { return biases; }
    public void setBiases(double[] biases) { this.biases = biases; }

    public void setPotentials(Matrix potentials) { this.potentials = potentials; }
    public Matrix getPotentials() { return potentials; }

    public void setOutputs(Matrix outputs) { this.outputs = outputs; }
    public Matrix getOutputs() { return this.outputs; }
    public ActivationFunction getActivationFunction() { return this.activationFunction; }

    public Matrix getGammas() { return gammas; }
    public void setGammas(Matrix gammas) { this.gammas = gammas; }

    public int getNeuronCount() { return neuronCount; }

    // ----------------------------------------------------------------

    /** Initialize all layer data. */
    private void prepare() {
        Pair<Double> limits = Functions.glorot(dimensions);
        initWeights(limits);
        initBiases(limits);
    }
    /** Initializes the weights with a random number from an interval. */
    private void initWeights(Pair<Double> limits) {
        ArrayList<double[]> W = new ArrayList<>();
        for (int i = 0; i < neuronCount; i++) {
            double[] neuronWeights = new double[dimensions.a];
            for (int j = 0; j < neuronWeights.length; j++) {
                neuronWeights[j] = MathUtils.randomDouble(limits);
            }
            W.add(neuronWeights);
        }
        weights = new Matrix(W);
    }

    /** Initializes the biases with a random number from an interval. */
    private void initBiases(Pair<Double> limits) {
        biases = new double[neuronCount];
        for (int i = 0; i < neuronCount; i++) {
            biases[i] = MathUtils.randomDouble(limits);
        }
    }
}
