package neural;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Locale;

/** A wrapper for a collection of heuristics to use in the neural network. */
public class Heuristics {

    EnumSet<Heuristic> hs = EnumSet.noneOf(Heuristic.class);

    // placeholder values!
    public double momentum = 5.0;
    public double decay = 1.0 / 4.0;

    public static final EnumSet<Heuristic> None = EnumSet.noneOf(Heuristic.class);
    public static final EnumSet<Heuristic> All = EnumSet.allOf(Heuristic.class);

    /** No additional heuristics will be used. */
    public Heuristics() { reset(); }

    public Heuristics(Heuristic... heuristics) {
        hs = EnumSet.noneOf(Heuristic.class);
        hs.addAll(Arrays.asList(heuristics));
    }

    /** Disables all heuristics and resets their parameters. */
    public void reset() {
        hs = EnumSet.noneOf(Heuristic.class);
        momentum = 5.0;
        decay = 1.0 / 4.0;
    }

    /** Enables momentum and sets its parameter. */
    public void setMomentum(double momentum) {
        if (!hs.contains(Heuristic.Momentum))
            hs.add(Heuristic.Momentum);
        this.momentum = momentum;
    }

    /** Enables weight decay and sets its parameter. */
    public void setDecay(double decay) {
        if (!hs.contains(Heuristic.WeightDecay))
            hs.add(Heuristic.WeightDecay);
        this.decay = decay;
    }

    // TODO: Dropout


    @Override
    public String toString() {
        StringBuilder sr = new StringBuilder();
        sr.append("{");
        int count = 1;
        if (hs.size() == 0) sr.append(" ");
        for (Heuristic h : hs) {
            sr.append(h.toString());
            if (h == Heuristic.Momentum) sr.append(String.format(Locale.ROOT," (%.1f)", momentum));
            if (h == Heuristic.WeightDecay) sr.append(String.format(Locale.ROOT,"(%.2f)", decay));
            if (count < hs.size()) sr.append(", ");
            count++;
        }
        sr.append("}");
        return sr.toString();
    }
}
