package neural;

/** Additional heuristics to use in the neural network. */
public enum Heuristic {
    Momentum, WeightDecay, Dropout
}