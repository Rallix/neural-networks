package neural;

/**
 * The type of a neural network layer.
 */
public enum LayerType {
    Input, Hidden, Output // TODO: Can be determined simply from the order
}
