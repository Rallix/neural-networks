package neural.functions;

import mathematics.Functions;

/**
 * The sigmoid activation function.
 */
public class Sigmoid implements ActivationFunction {


    @Override
    public double function(double value) {
        return Functions.sigmoid(value);
    }

    @Override
    public double functionDerivative(double value) {
        return Functions.sigmoidDerivative(value);
    }
}
