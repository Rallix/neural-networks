package neural.functions;

import mathematics.Functions;

/**
 * The rectified linear unit activation function.
 */
public class ReLU implements ActivationFunction {
    @Override
    public double function(double value) {
        return Functions.relu(value);
    }

    @Override
    public double functionDerivative(double value) {
        return Functions.reluDerivative(value);
    }
}
