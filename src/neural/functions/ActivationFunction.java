package neural.functions;

import mathematics.Functions;

/**
 * An activation function from the {@link Functions function} catalogue.
 */
public interface ActivationFunction {

    /**
     * The activation function.
     * @param value The potential.
     */
    double function(double value);

    /**
     * The derivative of the activation function.
     * @param value The potential.
     */
    double functionDerivative(double value);

    // TODO: https://medium.com/@alfonsollanes/why-do-we-use-the-derivatives-of-activation-functions-in-a-neural-network-563f2886f2ab
}
