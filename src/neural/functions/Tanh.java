package neural.functions;

import mathematics.Functions;

/**
 * The hyperbolic tangent activation function.
 */
public class Tanh implements ActivationFunction {


    @Override
    public double function(double value) {
        return Functions.tanh(value);
    }

    @Override
    public double functionDerivative(double value) {
        return Functions.tanhDerivative(value);
    }
}
