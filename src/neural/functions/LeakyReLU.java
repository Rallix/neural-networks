package neural.functions;

import mathematics.Functions;

/**
 * The leaky rectified linear unit activation function.
 */
public class LeakyReLU implements ActivationFunction {

    @Override
    public double function(double value) {
        return Functions.leakyRelu(value);
    }

    @Override
    public double functionDerivative(double value) {
        return Functions.leakyReluDerivative(value);
    }
}
