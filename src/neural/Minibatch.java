package neural;

import mathematics.Matrix;

import java.util.List;

/**
 * A unique part of the dataset.
 * Every learning epoch processes all the minibatches once.
 */
public class Minibatch {

    /** Inputs. */
    private Matrix inputs;

    /** Labels. */
    private Matrix labels;

    /** Predicted labels. */
    private List<Integer> predictions;

    /** The size of the minibatch. A portion of the dataset. */
    private int size;

    /** Denotes how far towards the epoch completion this batch is on scale {@code <0,1>}. */
    private double progress = -1;

    /** A constructor of the {@link Minibatch minibatch}, setting its size. */
    public Minibatch(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return String.format("Minibatch:\n inputs:\t%s\n labels:\t%s\n", inputs.toString(), labels.toString());
    }

    // Getters and setters --------------------------------------------

    public Matrix getInputs() {
        return inputs;
    }
    public void setInputs(Matrix inputs) {
        this.inputs = inputs;
    }
    public Matrix getLabels() {
        return labels;
    }
    public void setLabels(Matrix labels) {
        this.labels = labels;
    }
    public int getSize() { return size; }
    public void setSize(int size) { this.size = size; } // when the batch can't be full

    // Testing
    public List<Integer> getPredictions() { return predictions; }
    public void setPredictions(List<Integer> predictions) { this.predictions = predictions; }

    // Debug
    public double getProgress() {
        return progress;
    }
    public void setProgress(double progress) {
        this.progress = progress;
    }


    // ----------------------------------------------------------------
}
