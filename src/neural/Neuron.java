package neural;

// TODO: Delete; it's not very useful and matrix operations can't really be used.
// ---> Unused | "reduce depth of object hierarchy"
/** See <a href="https://www.youtube.com/watch?v=1DIu7D98dGo&list=WL&index=239&t=0s">tutorial</a>. */
public class Neuron {
    final double[] weights;
    final double bias;
    double value = 0;

    public Neuron() {
        this.weights = new double[0];
        this.bias = 0;
    }

    public Neuron(double[] weights, double bias) {
        this.weights = weights;
        this.bias = bias;
    }
}
