package tests;

import main.MultilayerPerceptron;
import neural.Layer;
import neural.Network;
import neural.functions.Sigmoid;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import utils.Pair;

public class NetworkTest {

    private MultilayerPerceptron mlp;
    @Before
    public void prepare() {
        mlp = new MultilayerPerceptron();
    }

    @Test
    public void createLayer_checkUnderlyingData() {
        Layer layer = new Layer(21, new Sigmoid(), new Pair<>(12, 7));
        Assert.assertEquals(layer.getBiases().length, layer.getNeuronCount());
        Assert.assertEquals(layer.getWeights().getRowCount(), 21);
        Assert.assertEquals(layer.getWeights().getRow(0).length, 12); // first row has 12 inputs
    }

    @Test
    public void createNetwork_topology() {
        int[] layers = new int[] {5, 8, 12};
        Network network = new Network(new Sigmoid(), 0.65, 64, layers);
        Assert.assertEquals(layers.length - 1, network.getLayers().size());
        Assert.assertEquals(network.getMinibatch().getSize(), 64);
        Pair<Integer> dims = network.getDimensions();
        Assert.assertEquals((int) dims.a, 5);
    }

}
