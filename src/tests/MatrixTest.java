package tests;

import mathematics.Functions;
import mathematics.MathUtils;
import mathematics.Matrix;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.function.DoubleUnaryOperator;

/**
 * Tests for {@link Matrix}.
 */
public class MatrixTest {
    private Matrix matrix;
    private Matrix matrix2;
    private Matrix smallMatrix;

    @Before
    public void prepare() {
        matrix =  new Matrix(new double[][] {{6,7,3},{1,2,3},{5,4,7}});
        matrix2 = new Matrix(new double[][] {{9,8,3},{1,9,3},{3,2,1}});
        smallMatrix = new Matrix(new double[][] {{4,3},{2,7}});
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_JaggedMatrixNotAllowed() {
        new Matrix(new double[][] {{4,3},{2,7,8}});
    }

    @Test
    public void transpose_smallMatrix()
    {
        final Matrix expected = new Matrix(new double[][]{{4,2},{3,7}});
        final Matrix actual = smallMatrix.transpose();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void add_matchingDimensions() {
        final Matrix expected = new Matrix(new double[][] {{15,15,6},{2,11,6},{8,6,8}});
        final Matrix actual = matrix.add(matrix2);

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = RuntimeException.class)
    public void add_wrongDimensions() {
        matrix.add(smallMatrix);
    }

    @Test
    public void subtract_matchingDimensions() {
        final Matrix expected = new Matrix(new double[][] {{3,1,0},{0,7,0},{-2,-2,-6}});
        final Matrix actual = matrix2.subtract(matrix);

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = RuntimeException.class)
    public void subtract_wrongDimensions() {
        matrix.subtract(smallMatrix);
    }

    @Test
    public void product_matchingDimensions() {
        final Matrix expected = new Matrix(new double[][] {{54,56,9},{1,18,9},{15,8,7}});
        final Matrix actual = matrix.product(matrix2);

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = RuntimeException.class)
    public void product_wrongDimensions() {
        matrix.product(smallMatrix);
    }

    @Test
    public void eq_equalObjects() {
        Assert.assertTrue(matrix.eq(matrix));
    }

    @Test
    public void eq_equalValues() {
        final Matrix other = new Matrix(new double[][] {{6,7,3},{1,2,3},{5,4,7}});

        Assert.assertTrue(matrix.eq(other));
    }

    @Test
    public void eq_equalValues_tinyDifference() {
        final double diff = 0.000_001;
        final Matrix other = new Matrix(new double[][] {{6,7 + diff,3},{1,2,3 + diff},{5 + diff,4,7}});

        Assert.assertTrue(matrix.eq(other));
    }

    @Test
    public void eq_unequalValues() {
        final Matrix other = new Matrix(new double[][] {{6,7,4},{1,2,3},{5,2,7}});

        Assert.assertFalse(matrix.eq(other));
    }

    @Test
    public void multiply_matchingDimensions() {
        final Matrix expected = new Matrix(new double[][] {{70,117,42},{20,32,12},{70,90,34}});
        final Matrix actual = matrix.multiply(matrix2);

        Assert.assertEquals(expected, actual);
    }

    @Test(expected = RuntimeException.class)
    public void multiply_wrongDimensions() {
        matrix.multiply(smallMatrix);
    }

    @Test
    public void multiply_scalar() {
        final double scalar = 6;
        final Matrix expected = new Matrix(new double[][] {{36,42,18},{6,12,18},{30,24,42}});
        final Matrix actual = matrix.multiply(scalar);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void sum() {
        final double expected = 38;
        final double actual = matrix.sum();

        Assert.assertEquals(expected, actual, MathUtils.Epsilon);
    }

    @Test
    public void addUp() {
        final double[] expected = new double[] {16, 6, 16};
        final double[] actual = matrix.addUpRows();

        Assert.assertArrayEquals(expected, actual, MathUtils.Epsilon);
    }

    @Test
    public void map_addOne() {
        final Matrix expected = new Matrix(new double[][] {{7,8,4},{2,3,4},{6,5,8}});
        final Matrix actual = matrix.map(d -> d + 1);

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void map_sigmoid() {
        DoubleUnaryOperator sigmoid = Functions::sigmoid;
        double[][] expectedMapped = new double[][] {{6,7,3},{1,2,3},{5,4,7}};
        for (int row = 0; row < expectedMapped.length; row++)
            for (int col = 0; col < expectedMapped[row].length; col++)
                expectedMapped[row][col] = sigmoid.applyAsDouble(expectedMapped[row][col]);
        final Matrix expected = new Matrix(expectedMapped);
        final Matrix actual = matrix.map(sigmoid);

        Assert.assertEquals(expected, actual);
    }

    // matrix =  new Matrix(new double[][] {{6,7,3},{1,2,3},{5,4,7}});
    // matrix2 = new Matrix(new double[][] {{9,8,3},{1,9,3},{3,2,1}});
}
