package utils;

/** A pair of arbitrary values. */
public class Pair<N extends Number> {
    public N a;
    public N b;

    public Pair() {}

    public Pair(N a, N b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", a.toString(), b.toString());
    }
}
