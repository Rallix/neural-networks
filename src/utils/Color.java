package utils;

/** Colors used in the console. */
// More colors: https://stackoverflow.com/a/45444716/10806555
public enum Color {
    Red, Green, Yellow, Blue, Purple, Cyan, White, Black;

    /** Adds a colour tint to a string. */
    public static String ColorString(String str, Color color) {
        String colorId;
        switch (color) {
            default: colorId = RESET; break;
            case White: colorId = WHITE; break;
            case Red: colorId = RED; break;
            case Green: colorId = GREEN; break;
            case Yellow: colorId = YELLOW; break;
            case Blue: colorId = BLUE; break;
            case Purple: colorId = PURPLE; break;
            case Cyan: colorId = CYAN; break;
            case Black: colorId = BLACK; break;
        }
        return String.format("%s%s%s", colorId, str, RESET);
    }

    // Reset
    private static final String RESET = "\033[0m";  // Text Reset

    // Regular Colors
    public static final String BLACK = "\033[0;30m";   // BLACK
    private static final String RED = "\033[0;31m";     // RED
    private static final String GREEN = "\033[0;32m";   // GREEN
    private static final String YELLOW = "\033[0;33m";  // YELLOW
    private static final String BLUE = "\033[0;34m";    // BLUE
    private static final String PURPLE = "\033[0;35m";  // PURPLE
    private static final String CYAN = "\033[0;36m";    // CYAN
    private static final String WHITE = "\033[0;37m";   // WHITE

}