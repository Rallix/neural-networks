package utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/** A class handling loading inputs. For saving data, see {@link main.MultilayerPerceptron#export(java.lang.String) mlp.export}. */
public final class FileUtils {
    /** Maximum amount of numbers that can be contained by a single array. */
    private static final int MAX_ARRAY_SIZE = Integer.MAX_VALUE - 8;

    /** A private constructor preventing instantiation. **/
    private FileUtils() {
        throw new UnsupportedOperationException();
    }

    /**
     * Loads inputs from a CSV file.
     * @param fileName CSV file name to load from.
     * @return A list of input vectors.
     * @throws IOException File cannot be found or otherwise worked with.
     */
    public static List<double[]> loadRawInputs(String fileName) throws IOException
    {
        Path path = Paths.get(fileName);
        List<double[]> inputs = new ArrayList<>();

        try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.US_ASCII)) {
            long lineCount = Files.lines(path).count();
            if (lineCount > MAX_ARRAY_SIZE)
                throw new IOException("The input vectors file is too large to handle at once.");

            String line = reader.readLine();
            while (line != null) {
                // Add all comma-separated numbers to the vector
                String[] lineInputs = line.split(",");
                double[] vector = new double[lineInputs.length];
                for (int i = 0; i < lineInputs.length; i++) {
                    vector[i] = Double.parseDouble(lineInputs[i]);
                }
                inputs.add(vector);
                line = reader.readLine();
            }
        }
        return inputs;
    }

    /**
     * Loads expected labels from a CSV file.
     * @param fileName CSV file name to load from.
     * @return A list of labels.
     * @throws IOException File cannot be found or otherwise worked with.
     */
    public static List<Integer> loadRawLabels(String fileName) throws IOException {
        Path path = Paths.get(fileName);
        List<Integer> labels = new ArrayList<>();

        try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.US_ASCII)) {
            String line = reader.readLine();
            while (line != null) {
                // Only a single label per line
                int label = Integer.parseInt(line);
                labels.add(label);
                line = reader.readLine();
            }
        }
        return labels;
    }
}
