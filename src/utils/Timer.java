package utils;

import java.time.Duration;
import java.time.Instant;

/** A simple timer measuring the time between {@code start()} and {@code stop()} methods. */
public class Timer {

    private Instant startTime = null;
    private Instant endTime = null;

    private Duration lastTime = Duration.ZERO;

    /** Start the timer. */
    public void start() {
        startTime = Instant.now();
        endTime = null;
    }

    /** Stop the timer and save the measured time. */
    public Duration stop() {
        if (startTime == null)
            throw new IllegalStateException("Attempting to stop the timer without starting it.");
        endTime = Instant.now();
        lastTime = Duration.between(startTime, endTime);
        return lastTime;
    }

    /** Return the last measured time. */
    public Duration getLastTime() {
        if (startTime == null || endTime == null) {
            Debug.LogError("Attempting to retrieve time without starting or stopping the timer first.");
            return Duration.ZERO;
        }
        return lastTime;
    }

    /** Returns the current elapsed time without stopping the timer.*/
    public Duration getCurrentTime() {
        if (startTime == null) {
            Debug.LogError("Attempting to retrieve time without starting the timer first.");
            return Duration.ZERO;
        }
        return Duration.between(startTime, Instant.now());
    }

}
