package utils;

import mathematics.MathUtils;

/**
 * Class containing shortcuts for printing to the console.
 */
public class Debug {

    /** Enable showing debug messages in the console? */
    public static boolean enable = true;

    /** How many tabs to prepend to the debug message. */
    private static int tabLevel = 0;

    public static void tab() { tabLevel = MathUtils.clamp(tabLevel + 1, 0, Integer.MAX_VALUE); }
    public static void untab() { tabLevel = MathUtils.clamp(tabLevel - 1, 0, Integer.MAX_VALUE); }

    /** Terminates the current line in the console. */
    public static void newLine() { System.out.println(); }

    /**
     * Logs a message to the console.
     */
    public static void Log(Object message) {
        if (enable) System.out.println(Indent(message.toString()));
    }

    /**
     * Logs a formatted message to the console.
     */
    public static void LogFormat(String format, Object... args) {
        if (enable) System.out.printf(Indent(format) + "\n", args);
    }

    /**
     * Logs a colored message to the console.
     */
    public static void LogColor(Object message, Color color) {
        if (enable) System.out.println(Indent(Color.ColorString(message.toString(), color)));
    }

    /**
     * Logs a colored, formatted message to the console.
     */
    public static void LogColorFormat(String format, Color color, Object... args) {
        if (enable) System.out.printf(Indent(Color.ColorString(format, color)) + "\n", args);
    }

    /**
     * A variant of {@link Debug#Log(Object)} that logs an error message to the console.
     */
    public static void LogError(Object message) {
        if (enable) System.err.println(Indent(message.toString()));
    }

    /**
     * A variant of {@link Debug#LogFormat(String, Object...)} that logs a formatted error message to the console.
     */
    public static void LogErrorFormat(String format, Object... args) {
        if (enable) System.err.printf(Indent(format) + "\n", args);
    }

    /**
     * A variant of {@link Debug#Log(Object)} that logs an error message from an exception to the console.
     */
    public static void LogException(Exception e) {
        if (enable) e.printStackTrace();
    }

    /** Indents a message based on the global tab level. */
    private static String Indent(String string) {
        StringBuilder sr = new StringBuilder();
        for (int i = 0; i < tabLevel; i++) {
            sr.append("\t");
        }
        sr.append(string);
        return sr.toString();
    }
}
