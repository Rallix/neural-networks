package main;

import mathematics.Functions;
import mathematics.MathUtils;
import mathematics.Matrix;
import neural.Heuristics;
import neural.Minibatch;
import neural.Network;
import neural.functions.ActivationFunction;
import utils.Color;
import utils.Debug;
import utils.Timer;

import java.io.*;
import java.util.*;

import static main.Mnist.*;

/**
 * A wrapper around the {@link Network Network} class,
 * handling its interaction with inputs, outputs and time measurement.
 */
public class MultilayerPerceptron {

    // TODO: Measure time < 30 min!

    private Network network;

    private Matrix inputs;
    private Matrix labels;

    private Heuristics heuristics;

    /** Access points to inputs. Usually shuffled. */
    private Queue<Integer> inputIndexes = new LinkedList<>(); // for shuffling
    private boolean enableShuffling = true;

    /** The initially set size of a batch. */
    private int defaultBatchSize;

    /** Time measurement */
    private final Timer timer = new Timer();

    private String outputFile;

    // Getters and setters --------------------------------------------

    public void enableShuffling(boolean enable) { this.enableShuffling = enable; }

    // ----------------------------------------------------------------

    /**
     * Creates a multilayer perceptron network.
     * Preprocessing of the values before they're passed to the {@link neural.Network Network} class.
     * @param activationFunction An activation function of the network.
     * @param learningRate The learning rate of the network.
     * @param minibatchSize The size of a {@link Minibatch minibatch}.
     * @param layers The topology of the network (from input, through hidden, to output layers).
     * @apiNote Example: mlp.create(new Sigmoid(), 64, 784, 30, 30, 10);
     */
    public void create(ActivationFunction activationFunction, double learningRate, int minibatchSize, int... layers) {
        // clamp minibatch size within reasonable limits
        minibatchSize = MathUtils.clamp(minibatchSize, 1, LINE_COUNT);
        this.defaultBatchSize = minibatchSize;
        network = new Network(activationFunction, learningRate, minibatchSize, layers);
    }

    // Input data -----------------------------------------------------

    /** Initializes inputs and scales them. */
    public void processInputs(List<double[]> rawInputs) {
        for (double[] input : rawInputs) {
            // Remap <0,255> to <0,1>
            MathUtils.map(input, Functions::remapGreyscale);
        }
        this.inputs = new Matrix(rawInputs);
    }

    /** Initializes labels. */
    public void processLabels(List<Integer> rawLabels) {
        // label we're looking for -> 1, everything else -> 0
        List<double[]> labels = new ArrayList<>();
        for (int label : rawLabels) {
            double[] vector = new double[network.getDimensions().b]; // output dimensions
            vector[label] = 1.0;
            labels.add(vector);
        }
        this.labels = new Matrix(labels);
    }

    // ----------------------------------------------------------------

    // Learning (minibatches) -----------------------------------------

    /** Extracts some inputs from the stack and fills a batch. */
    public void initializeMinibatch() {
        // Set progress
        int remaining = inputIndexes.size();
        double progress = (inputs.getRowCount() - remaining) / (double) inputs.getRowCount();
        network.getMinibatch().setProgress(progress);
        // Pour data into the minibatch according to the (shuffled) indexes
        ArrayList<double[]> minibatchInputs = new ArrayList<>();
        ArrayList<double[]> minibatchLabels = new ArrayList<>();
        for (int i = 0; i < network.getMinibatch().getSize(); i++) {
            int current = inputIndexes.remove();
            minibatchInputs.add(inputs.getRow(current));
            minibatchLabels.add(labels.getRow(current));
        }
        network.getMinibatch().setInputs((new Matrix(minibatchInputs)).transpose());
        network.getMinibatch().setLabels((new Matrix(minibatchLabels)).transpose());
    }

    /** Learning algorithms for a single minibatch. */
    public void processMinibatch() {
        //! -- Bookmark.
        initializeMinibatch();
        network.feedForward();
        network.backPropagation();
        network.gradientDescent();
    }

    /** Tests the accuracy and exports the result.
     * @param save Save the results to a file.
     * @return The total of correctly predicted labels. */
    public int testMinibatch(boolean save) {
        initializeMinibatch();
        network.feedForward();
        network.predict();

        int correct = 0;
        Matrix expectedLabels = network.getMinibatch().getLabels().transpose();
        for (int i = 0; i < network.getMinibatch().getSize(); i++) {
            double[] row = expectedLabels.getRow(i);
            int prediction = network.getMinibatch().getPredictions().get(i);
            int expected = -1;
            for (int j = 0; j < row.length; j++) {
                if (row[j] == 1) {
                    expected = j;
                    if (j == prediction) correct++;
                    break;
                }
            }
            if (save && expected >= 0) {
                save(expected, prediction);
            }
        }
        return correct;
    }

    // ----------------------------------------------------------------

    // Epochs & dividing into minibatches -----------------------------

    /**
     * Initiate the learning algorithm for the specified number of epochs.
     * @param epochs The number of times the whole dataset is processed
     * @param heuristics Additional heuristics to improve the learning rate.
     */
    public void learn(int epochs, Heuristics heuristics) {
        this.heuristics = heuristics;
        for (int i = 0; i < epochs; i++) {
            Debug.LogColorFormat("%d. Epoch", Color.Yellow, i+1);
            Debug.tab();
                timer.start();
                inputIndexes = getShuffledIndexes(inputs.getRowCount(), enableShuffling);
                learnProcess();
                Debug.LogColorFormat("Finished in %d seconds.", Color.Red, timer.stop().getSeconds());
                // Check accuracy every now and then
                if ((i+1) % 5 == 0 || i == 0) {
                    if ((i+1) == epochs) continue; // final epoch -> don't test accuracy, it will be tested right away
                    checkAccuracy();
                }
            Debug.untab();
        }
    }

    /** A single epoch of the learning process. */
    private void learnProcess() {
        // Process all "full" batches
        while (inputIndexes.size() >= network.getMinibatch().getSize()) {
            processMinibatch();
        }
        // Process the remainder, and reset the size after for the following epochs
        int remaining = inputIndexes.size();
        if (remaining != 0) {
            network.getMinibatch().setSize(remaining); // the rest
            processMinibatch();
            network.getMinibatch().setSize(defaultBatchSize);
        }
    }

    /** Creates a shuffled stack of indexes in range {@code <0,totalSize-1>}. */
    public Queue<Integer> getShuffledIndexes(int totalSize, boolean shuffled) {
        List<Integer> sequence = MathUtils.sequence(0, totalSize);
        if (shuffled) Collections.shuffle(sequence);
        return new LinkedList<>(sequence);
    }

    // ----------------------------------------------------------------

    // Accuracy and exporting results ---------------------------------

    public void checkAccuracy() { checkAccuracy(false); }
    public void checkAccuracy(boolean exportFile) {
        inputIndexes = getShuffledIndexes(inputs.getRowCount(), enableShuffling);

        // Process all "full" batches
        int correctlyPredicted = 0;
        while (inputIndexes.size() >= network.getMinibatch().getSize()) {
            int correct = testMinibatch(exportFile);
            correctlyPredicted += correct;
        }
        // Process the remainder, and reset the size after for the following epochs
        int remaining = inputIndexes.size();
        if (remaining != 0) {
            network.getMinibatch().setSize(remaining); // the rest
            int correct = testMinibatch(exportFile);
            correctlyPredicted += correct;
            network.getMinibatch().setSize(defaultBatchSize);
        }

        Debug.Log(Color.ColorString("Predictions: ", Color.Purple) +
                Color.ColorString("Correct = " + correctlyPredicted + ", Wrong = " + (inputs.getRowCount() - correctlyPredicted), Color.Red));
        double accuracy = (((double) correctlyPredicted) / (inputs.getRowCount())) * 100;
        Debug.LogColorFormat("Accuracy = %.3f %%", Color.Yellow, accuracy);
    }

    /** Checks the accuracy of the model and exports the predictions.
     * @param outputFile The name of the exported predictions. */
    public void export(String outputFile) {
        this.outputFile = outputFile;
        try {
            // Clear files if they exist
            new PrintWriter(ExportFolder + outputFile).close();
        } catch (FileNotFoundException fe) { /* It's fine */ }
        checkAccuracy(true);
    }

    /** Saves the labels to their corresponding files. */
    public void save(int expected, int actual) {
        // save predictions to 'actual'
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(ExportFolder + outputFile, true))) {
            writer.write(String.valueOf(actual));
            writer.newLine();
        } catch (IOException ioe) {
            Debug.LogError("Error when opening the predictions output file.");
            Debug.LogException(ioe);
        }
    }

    // ----------------------------------------------------------------

}
