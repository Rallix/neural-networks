package main;

import neural.Heuristics;
import neural.functions.Sigmoid;
import utils.Color;
import utils.Debug;
import utils.FileUtils;
import utils.Timer;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;

/** The main entry point to the program. */
public class Mnist {

    /** The number count in a line in MNIST.
     *  28x28 pixels, 0-255 greyscale.
     *  See <a href="https://youtu.be/aircAruvnKk">here</a> for details.
     */
    public static final int PIXEL_COUNT = 784;
    /** A dull constant denoting the number of available digits of a DEC number. */
    public static final int DIGIT_COUNT = 10;
    /** The number of neurons in hidden layers. */
    public static final int HIDDEN = 64; // *2
    /** The size of a {@link neural.Minibatch minibatch} to process. */
    public static final int MINIBATCH_SIZE = 128; // *2
    /** The total number of lines in MNIST. */
    public static final int LINE_COUNT = 60_000; // not necessary; scan to the end
    /** The learning rate of the neural network. */
    public static final double LEARNING_RATE = 0.60;
    /** The number of times to process the entire dataset . */
    public static final int EPOCHS = 25;

    // Filepaths
    public static final String MNISTFolder = "MNIST_DATA/";
    public static final String TrainInputsPath = MNISTFolder + "mnist_train_vectors.csv";
    public static final String TrainLabelsPath = MNISTFolder + "mnist_train_labels.csv";
    public static final String TestInputsPath = MNISTFolder + "mnist_test_vectors.csv";
    public static final String TestLabelsPath = MNISTFolder + "mnist_test_labels.csv";
    // Exports | "on each line is only one number present"
    public static final String ExportFolder = "./"; // "pv021_project/"
    public static final String TrainPredictionsFile = ExportFolder + "trainPredictions";
    public static final String TestPredictionsFile = ExportFolder + "actualTestPredictions";

    // Evaluator
    public static final String AutomaticEvaluator = "pv021_project/fi-muni-pv021-automatic-evaluator-1.0-SNAPSHOT-jar-with-dependencies.jar";

    // Timing
    static final Timer globalTimer = new Timer();

    /** Train and test a neural network on the MNIST dataset.
     * @param args [epochs] Accepts a single number denoting the number of training epochs. */
    public static void main(String... args) {
        // Epoch size as an optional argument
        int epochs = EPOCHS;
        if (args.length == 1) {
            try {
                epochs = Integer.parseInt(args[0]);
            } catch (NumberFormatException nfe) { /* Wrong argument */ }
        }

        Debug.enable = true; // show debug prints in the console
        Debug.LogColor("Neural Network | MNIST", Color.Red);
        globalTimer.start();

        mnist(epochs);

        Duration duration = globalTimer.stop();
        Debug.LogColorFormat("Total elapsed time: %d minutes %d seconds", Color.Yellow,
                duration.toMinutes(), duration.getSeconds() % 60);

        // Run the automatic evaluator
        // runEvaluator(TestPredictionsFile, TestLabelsPath);
    }

    /** Train and test with the MNIST dataset. */
    public static void mnist(int epochs) {
        // 1) Create network
        MultilayerPerceptron mlp = new MultilayerPerceptron();
        // 784 -- 64 --- 64 --- 10
        int[] topology = {PIXEL_COUNT, HIDDEN, HIDDEN, DIGIT_COUNT}; // 16: "about 96% accuracy"
        Debug.LogColorFormat("Creating a network: %s", Color.Purple,
                Color.ColorString(Arrays.toString(topology), Color.Cyan));
        Debug.LogColorFormat("Minibatch size: %s", Color.Purple,
                Color.ColorString(String.valueOf(MINIBATCH_SIZE), Color.Cyan));
        Debug.LogColorFormat("Planned epochs: %s", Color.Purple,
                Color.ColorString(String.valueOf(epochs), Color.Yellow));
        mlp.create(new Sigmoid(), LEARNING_RATE, MINIBATCH_SIZE, topology);

        // 2) Load train data
        if (epochs > 0) {
            try {
                Debug.LogColor("Processing input data...", Color.Purple);
                Timer fileTimer = new Timer(); fileTimer.start();
                mlp.processInputs(FileUtils.loadRawInputs(TrainInputsPath));
                mlp.processLabels(FileUtils.loadRawLabels(TrainLabelsPath));
                Debug.tab(); Debug.LogColorFormat("Finished in %d seconds", Color.Red, fileTimer.stop().getSeconds()); Debug.untab();
            } catch (IOException io) {
                Debug.LogException(io);
                return;
            }

        // 3) Learn
            Heuristics heuristics = new Heuristics(); // TODO: Use more heuristics
            Debug.Log(Color.ColorString("Using heuristics:", Color.Purple) + " " + heuristics.toString()
                    + Color.ColorString(", learning rate ", Color.Purple) + Color.ColorString(String.format("%.2f", LEARNING_RATE), Color.Cyan));
            // TODO: Beware | Too many epochs --> overfitting | https://towardsdatascience.com/epoch-vs-iterations-vs-batch-size-4dfb9c7ce9c9
            mlp.enableShuffling(false);
            mlp.learn(epochs, heuristics);
            mlp.export(TrainPredictionsFile); // train predictions
        }

        Debug.untab();
        Debug.LogColor("Testing...", Color.Purple);

        // 4) Test
        mlp.enableShuffling(false); // "do not shuffle testing data"
        try {
            mlp.processInputs(FileUtils.loadRawInputs(TestInputsPath));
            mlp.processLabels(FileUtils.loadRawLabels(TestLabelsPath));
        } catch (IOException io) {
            Debug.LogException(io);
            return;
        }
        mlp.export(TestPredictionsFile); // test predictions
    }

    /** Runs the automatic evaluator on the exported files */
    public static void runEvaluator(String actualFile, String expectedFile) {
        try {
            String evaluator = "\"" + AutomaticEvaluator + "\"";
            String actual = "\"" + actualFile + "\"";
            String expected = "\"" + expectedFile + "\"";
            ProcessBuilder pb = new ProcessBuilder("java", "-jar", evaluator,
                    actual, expected, String.valueOf(DIGIT_COUNT));
            Debug.newLine();
            Debug.LogColorFormat("Running command:\n%s %s %s %s", Color.Purple,
                    Color.ColorString("java -jar", Color.Yellow),
                    Color.ColorString(evaluator, Color.Green),
                    Color.ColorString(actual + " " + expected, Color.Yellow),
                    Color.ColorString(String.valueOf(DIGIT_COUNT), Color.Cyan));
            // Start and wait until it completes the execution
            Process eval = pb.start();
            try {
                eval.waitFor();
            } catch (InterruptedException ie) {
                Debug.LogException(ie);
            }
        } catch (IOException ioe) {
            Debug.LogError("Couldn't open the automatic evaluator.");
        }
    }
}
