package mathematics;

import utils.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.DoubleUnaryOperator;

/**
 * A standard {@link Double} two-dimensional non-jagged matrix.
 */
public final class Matrix {
    private final double[][] data;
    private final int rows;
    private final int cols;

    // Getters --------------------------------------------------------

    public double[][] getData() {
        return data;
    }
    public double[] getRow(int row) {return data[row]; }
    public double getElement(int row, int col) { return data[row][col]; }
    public void setElement(int row, int col, double value) { data[row][col] = value; }

    public int getRowCount() { return rows; }
    public int getColCount() {
        return cols;
    }
    public Pair<Integer> size() { return new Pair<>(getRowCount(), getColCount()); }

    // ----------------------------------------------------------------

    /**
     * Creates an empty {@link Matrix matrix} of specified size.
     */
    public Matrix(int rows, int cols) {
        data = new double[rows][cols];
        this.rows = rows;
        this.cols = cols;
    }

    /**
     * Creates a {@link Matrix matrix} out of an array of doubles.
     */
    public Matrix(double[][] data) {
        rows = data.length;
        cols = data[0].length;
        this.data = new double[rows][cols];
        for (int row = 0; row < rows; row++) {
            if (data[row].length != cols)
                throw new IllegalArgumentException("Jagged matrices are not supported.");
            System.arraycopy(data[row], 0, this.data[row], 0, cols);
        }
    }

    /**
     * Creates a {@link Matrix matrix} out of a list of arrays.
     */
    public Matrix(List<double[]> data) {
        rows = data.size();
        cols = data.get(0).length;
        this.data = new double[rows][cols];
        for (int row = 0; row < rows; row++) {
            if (data.get(row).length != cols)
                throw new IllegalArgumentException("Jagged matrices are not supported.");
            System.arraycopy(data.get(row), 0, this.data[row], 0, cols);
        }
    }

    /**
     * Creates a {@link Matrix matrix} out of a list of lists.
     */
    public Matrix(List<List<Double>> data, int a) {
        rows = data.size();
        cols = data.get(0).size();
        this.data = new double[rows][cols];
        for (int row = 0; row < rows; row++) {
            if (data.get(row).size() != cols)
                throw new IllegalArgumentException("Jagged matrices are not supported.");
            for (int col = 0; col < cols; col++) {
                 this.data[row][col] = data.get(row).get(col);
            }
        }
    }

    /**
     * Creates a deep copy of another {@link Matrix matrix}.
     */
    public Matrix(Matrix matrix) {
        rows = matrix.data.length;
        cols = matrix.data[0].length;
        this.data = new double[rows][cols];
        for (int row = 0; row < rows; row++) {
            if (matrix.data[row].length != cols)
                throw new IllegalArgumentException("Jagged matrices are not supported.");
            System.arraycopy(matrix.data[row], 0, this.data[row], 0, cols);
        }
    }

    /**
     * Returns the matrix in a human-readable form.
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int row = 0; row < rows; ++row) {
            if (row != 0) sb.append(" "); // space: not first
            sb.append("[");
            for (int col = 0; col < cols; ++col) {
                sb.append(String.format("%4.2f", data[row][col]));
                if (col != cols - 1) sb.append(" "); // space: not last
            }
            sb.append("]");
            if (row == rows - 1) sb.append("]");
        }
        return sb.toString();
    }

    /**
     * Transposes the matrix (switches rows with columns).
     */
    public Matrix transpose() {
        Matrix result = new Matrix(cols, rows);
        for (int row = 0; row < rows; row++)
            for (int col = 0; col < cols; col++)
                result.data[col][row] = data[row][col];
        return result;
    }

    /**
     * Adds a matrix to the current matrix.
     *
     * @param b The matrix to add.
     * @throws RuntimeException when the dimensions of the matrices don't match.
     */
    public Matrix add(Matrix b) {
        Matrix a = this;
        if (a.rows != b.rows || a.cols != b.cols)
            throw new RuntimeException("The matrices have different dimensions and cannot be added.");
        Matrix result = new Matrix(rows, cols);
        for (int row = 0; row < rows; row++)
            for (int col = 0; col < cols; col++)
                result.data[row][col] = a.data[row][col] + b.data[row][col];
        return result;
    }

    /**
     * Subtracts a matrix from the current matrix.
     *
     * @param b The matrix to subtract.
     * @throws RuntimeException when the dimensions of the matrices don't match.
     */
    public Matrix subtract(Matrix b) {
        Matrix a = this;
        if (a.rows != b.rows || a.cols != b.cols)
            throw new RuntimeException("The matrices have different dimensions and cannot be subtracted.");
        Matrix result = new Matrix(rows, cols);
        for (int row = 0; row < rows; row++)
            for (int col = 0; col < cols; col++)
                result.data[row][col] = a.data[row][col] - b.data[row][col];
        return result;
    }

    /**
     * Multiply a matrix with the current matrix.
     *
     * @param b The matrix to multiply with.
     * @throws RuntimeException when the number of columns in A doesn't match the number of rows in B.
     */
    public Matrix multiply(Matrix b) {
        Matrix a = this;
        if (a.cols != b.rows)
            throw new RuntimeException("The matrices cannot be multiplied because their rows and column count doesn't match.");
        Matrix result = new Matrix(a.rows, b.cols);
        for (int row = 0; row < result.rows; row++)
            for (int col = 0; col < result.cols; col++)
                for (int i = 0; i < a.cols; i++)
                    result.data[row][col] += a.data[row][i] * b.data[i][col];
        return result;
    }

    /**
     * Multiplies a matrix with a scalar value.
     * @param number A numerical value.
     */
    public Matrix multiply(double number) {
        return this.map(d -> d * number);
    }

    /**
     * Is the matrix equal to another matrix?
     */
    public boolean eq(Matrix b) {
        Matrix a = this;
        if (a.rows != b.rows || a.cols != b.cols)
            return false;
        final double epsilon = MathUtils.Epsilon;
        for (int row = 0; row < rows; row++)
            for (int col = 0; col < cols; col++)
                if (!MathUtils.approximately(a.data[row][col], b.data[row][col]))
                    return false;
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Matrix matrix = (Matrix) o;
        return rows == matrix.rows &&
                cols == matrix.cols &&
                eq(matrix);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(rows, cols);
        result = 31 * result + Arrays.hashCode(data);
        return result;
    }

    /**
     * Multiplies a matrix element-wise with the current matrix.
     *
     * @param b The matrix to multiply with.
     * @throws RuntimeException when the dimensions of the matrices don't match.
     */
    public Matrix product(Matrix b) {
        Matrix a = this;
        if (a.rows != b.rows || a.cols != b.cols)
            throw new RuntimeException("The matrices have different dimensions and their elements cannot be multiplied.");
        Matrix result = new Matrix(rows, cols);
        for (int row = 0; row < rows; row++)
            for (int col = 0; col < cols; col++)
                result.data[row][col] = a.data[row][col] * b.data[row][col];
        return result;
    }

    /**
     * Sums all the values in a matrix.
     */
    public double sum() {
        double result = 0.0;
        for (int row = 0; row < rows; row++)
            for (int col = 0; col < cols; col++)
                result += data[row][col];
        return result;
    }

    /**
     * Adds up all the values in columns, leaving only rows.
     */
    public double[] addUpRows() {
        Matrix a = this;
        double[] result = new double[a.rows];
        for (int row = 0; row < a.rows; row++) {
            double total = 0;
            for (int col = 0; col < a.cols; col++) {
                total += a.data[row][col];
            }
            result[row] = total;
        }
        return result;
    }

    /**
     * Maps a function to every value of the matrix.
     *
     * @param function A lambda function to apply to each value.
     */
    public Matrix map(DoubleUnaryOperator function) {
        Matrix a = this;
        Matrix result = new Matrix(a);
        MathUtils.map(result.data, function);
        return result;
    }
}
