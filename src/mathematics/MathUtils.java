package mathematics;

import utils.Pair;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.function.DoubleUnaryOperator;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public final class MathUtils {

    /** The precision required to make {@link Double} equal. */
    public static final double Epsilon = 0.0001;

    /** Finds the maximal value in an array. */
    public static double max(double[] values) {
        double max = values[0];
        for (double value : values) {
            if (value > max) max = value;
        }
        return max;
    }
    /** Finds the index of the maximal value */
    public static int maxIndex(double[] values) {
        double maxValue = values[0];
        int maxIndex = 0;
        for (int i = 1; i < values.length; i++) {
            if (values[i] > maxValue) {
                maxIndex = i;
                maxValue = values[i];
            }
        }
        return maxIndex;
    }

    /**
     * Decides whether a value is approximately equal to another value.
     * @param a A value to be equal to {@code b}.
     * @param b A value to be equal to {@code a}.
     */
    public static boolean approximately(double a, double b) {
        return Math.abs(a - b) < Epsilon;
    }

    /**
     * Clamps a value within boundaries.
     * @param value Value to clamp.
     * @param lower The lower bound.
     * @param upper The upper bound.
     * @return A clamped value.
     */
    public static double clamp(double value, double lower, double upper) { return Math.min(Math.max(value, lower), upper); }

    /**
     * Clamps a value within boundaries.
     * @param value Value to clamp.
     * @param lower The lower bound.
     * @param upper The upper bound.
     * @return A clamped value.
     */
    public static int clamp(int value, int lower, int upper) {
        return Math.min(Math.max(value, lower), upper);
    }

    /**
     * Clamps a value within {@code <0,1>}.
     * @param value Value to clamp.
     * @return A clamped value.
     */
    public static double clamp01(double value) { return clamp(value, 0, 1); }

    /**
     * Remaps a value proportionally from range <{@code from1}, {@code to1}> to range <{@code from2}, {@code to2}>.
     * @param value The value to remap.
     * @param from1 The lower bound of the first range.
     * @param to1 The upper bound of the first range.
     * @param from2 The lower bound of the second range.
     * @param to2 The upper bound of the second range.
     * @return A remapped value.
     */
    public static double remap(double value, double from1, double to1, double from2, double to2) {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

    /**
     * Remaps a value proportionally from {@code range1} to {@code range2}.
     * @param value The value to remap.
     * @param range1 The first range.
     * @param range2 The second range.
     * @return A remapped value.
     */
    public static double remap(double value, Pair<Double> range1,  Pair<Double> range2) {
        return remap(value, range1.a, range1.b, range2.a, range2.b);
    }

    /**
     * Remaps a value proportionally from {@code range} to <{@code 0}, {@code 1}>.
     * @param value The value to remap.
     * @param from The lower bound to remap from.
     * @param to The upper bound to remap from.
     * @return A remapped value in the range of <{@code 0}, {@code 1}>.
     */
    public static double remap01(double value, double from, double to) {
        return remap(value, from, to, 0, 1);
    }

    /**
     * Remaps a value proportionally from {@code range} to <{@code 0}, {@code 1}>.
     * @param value The value to remap.
     * @param range The first range.
     * @return A remapped value.
     */
    public static double remap01(double value, Pair<Double> range) {
        return remap(value, range.a, range.b, 0, 1);
    }

    /**
     * Maps a function to every element of a given array.
     * @param array An array to apply the function to.
     * @param function A function to apply to the array.
     */
    public static void map(double[] array, DoubleUnaryOperator function) {
        for (int i = 0; i < array.length; i++) array[i] = function.applyAsDouble(array[i]);
    }

    /**
     * Maps a function to every element of a given two-dimensional array.
     * @param array2D A two-dimensional array to apply the function to.
     * @param function A function to apply to the two-dimensional array.
     */
    public static void map(double[][] array2D, DoubleUnaryOperator function) {
        for (double[] array : array2D) map(array, function);
    }

    /**
     * Creates an integer sequence from {@code from} (including) to {@code to} (excluding).
     * @param from The lower bound of the sequence (inclusive).
     * @param to The upper bound of the sequence (exclusive).
     */
    public static List<Integer> sequence(int from, int to) {
        return IntStream.rangeClosed(from, to-1)
                .boxed().collect(Collectors.toList());
    }

    /** Returns a random {@link java.lang.Double double} within specified limits. */
    public static double randomDouble(Pair<Double> limits) {
        return ThreadLocalRandom.current().nextDouble(limits.a, limits.b);
    }

    public static double meanSquareError(double expected, double actual) {
        return Math.pow(expected - actual, 2);
    }

    /** Subtracts array {@code b} from array {@code a}. */
    public static void subtractAll(double[] a, double[] b) {
        if (a.length != b.length)
            throw new RuntimeException("The arrays cannot be subtracted because their element count doesn't match.");
        for (int i = 0; i < a.length; i++) {
            a[i] -= b[i];
        }
    }
}
