package mathematics;

import utils.Pair;

import java.util.function.DoubleUnaryOperator;

// TODO: Sigmoid seems to work better than ReLU.

public final class Functions {

    // Private constructor to prevent instantiation
    private Functions() {
        throw new UnsupportedOperationException();
    }

    /**
     * The sigmoid mathematical function.
     *
     * @value The potential.
     */
    public static double sigmoid(double value) {

        return 1.0 / (1.0 + Math.exp(-value));
    }

    /**
     * The derivative of the {@link #sigmoid} function.
     *
     * @value The potential.
     */
    public static double sigmoidDerivative(double value) {

        return sigmoid(value) * (1 - sigmoid(value));
    }

    /**
     * The sigmoid mathematical function.
     *
     * @value The potential.
     */
    public static double tanh(double value) {

        return Math.tanh(value);
    }

    /**
     * The derivative of the {@link #tanh} function.
     *
     * @value The potential.
     */
    public static double tanhDerivative(double value) {
        double tanh = Math.tanh(value);
        return (1 / tanh) - tanh;
    }

    /**
     * ReLU activation function.
     *
     * @value The potential.
     */
    public static double relu(double value) {

        return Math.max(0, value);
    }

    /**
     * The derivative of the {@link #relu ReLU} function.
     *
     * @value The potential.
     */
    public static double reluDerivative(double value) {

        return (value > 0) ? 1 : 0;
    }

    /**
     * Leaky ReLU activation function.
     *
     * @value The potential.
     */
    public static double leakyRelu(double value) {
        return (value > 0) ? value : value * 0.01;
    }

    /**
     * The derivative of the {@link #leakyRelu Leaky ReLU} function.
     *
     * @value The potential.
     */
    public static double leakyReluDerivative(double value) {
        if (MathUtils.approximately(0, value)) return 0; // = 0
        else return (value > 0) ? 1 : 0.01;
    }

    /**
     * The softmax function (normalized exponential).
     *
     * @param values An array of potential.
     */
    public static double[] softmax(double[] values) {
        double max = MathUtils.max(values);
        for (int i = 0; i < values.length; i++) {
            values[i] = values[i] - max;
        }

        double total = 0;
        for (double value : values) {
            total += Math.exp(value);
        }

        double[] result = new double[values.length];
        for (int i = 0; i < result.length; i++) {
            result[i] = Math.exp(values[i]) / total;
        }
        return result;
    }

    /**
     * The derivative of the {@link #softmax} function (normalized exponential).
     *
     * @param values An array of potential.
     */
    public static double[] softmaxDerivative(double[] values) {
        // TODO: Max?
        // https://stackoverflow.com/q/57631507/10806555

        double total = 0;
        for (double value : values) {
            total += Math.exp(value);
        }

        double[] result = new double[values.length];
        for (int i = 0; i < result.length; i++) {
            double indexValue = Math.exp(values[i]);
            result[i] = indexValue * (1 / total - (total * total));
        }
        return result;
    }

    /** Remaps a greyscale color from <{@code 0}, {@code 255}> to <{@code 0}, {@code 1}>. */
    public static double remapGreyscale(double value) {
        return MathUtils.remap01(value, 0, 255);
    }

    /** Glorot weight initialization.
     * https://jamesmccaffrey.wordpress.com/2017/06/21/neural-network-glorot-initialization/ */
    public static Pair<Double> glorot(Pair<Integer> dimensions) {
        double limit = Math.sqrt(6 / (double) (dimensions.a + dimensions.b));
        return new Pair<>(-limit, limit);
    }

}
