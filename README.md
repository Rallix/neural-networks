# PV021: Neural Networks project

## Multilayer Perceptron

### Requirements

Your task is simple. Implement a feed-forward neural network in C/C++/Java/C# and
train it on a given dataset using a backpropagation algorithm. The dataset is
well known and quite easy - MNIST [[1]](http://yann.lecun.com/exdb/mnist/). A Dataset in CSV format will be uploaded
in IS in study materials. There are four data files. Two data files as input
vectors and two data files with a list of expected predictions.

The deadline is set for November 30th, 2019.

Some rules about implementations:

1. Your implementation must be compilable and runnable on the Aisa server.
2. The project must contain a runnable script called "RUN" which compiles,
executes and exports everything on "one-click".
3. The implementation must export vectors of train and test predictions.

    * Exported predictions must be in the same format as is
"actualPredictionsExample" file - on each line is only one number present.
Such number on i-th line represents predicted class index (there are classes
0 - 9 for MNIST) for i-th input vector, hence prediction order is relevant.
    * Name the exported files "trainPredictions" and "actualTestPredictions".
4. The implementation will take both train and test input vectors, but it must
not touch test data except the evaluation of the already trained model.

    * If any implementation will touch given test data before the evaluation
of the already trained model, it will be automatically marked as a failed
project.
    * Why is that - an optimal scenario would hide for you any test data, but
in that case, you would have to deal with serialization and deserialization of
your implementations or you would have to be bound to some given interface and
this is just not wanted in this course.
    * Don't cheat, your implementations will be checked.
5. It's demanded to reach at least 95% of correct test predictions
(overall accuracy) with given at most half an hour of training time.
    * Implementations will be executed for a little longer, let's say for 35
minutes. At that time, it should be able to load the data, process them,
train the model and export train/test predictions out to files.
6. The correctness will be checked using an independent program which will be
also
provided for your own purposes.
7. The use of high-level libraries is forbidden. In fact, you don't need any.
    * Of course, you can use low-level libraries. You definitely can use basic
mathematics functions like exp, sqrt, log, rand, etc. High-level libraries are such
libraries containing matrix-based operations, neural network tools such as
already implemented layers with activation functions, automatic differentiation,
equation/linear-program solvers, etc.
8. What you do internally with the training dataset is up to you.
9. Pack all data with your implementations and put them on the right path so
your program will load them correctly on AISA (project dir. is fine).

You can make your own implementation or you can make teams of two. If there are
any problems, don't hesitate to contact me. If you are struggling with
network performance, contact me.

********************************************************************************
Please, do note that my time is also limited and this project is not the easiest
one so start "as asap as possible" and if you encounter any problems contact me
immediately. I won't have time for half of you day before the deadline.
********************************************************************************

### FAQ
 - "Can I write in Python, please, please, pretty please?"
     - no.
 - "Can I instead of the feed-forward implement a convolutional
 neural network?"
     - yes, but it might be much harder.
 - "Have Java implementations chance against C implementations?"
     - yes. Two years ago, the best performing implementation was written in
 java.

### Some tips

- solve the XOR problem first. XOR is a very nice example as a benchmark of the
working learning process with at least one hidden layer. Btw, presented network
solving XOR on the lecture is minimal and it can be hard to find, so consider
more neurons in the hidden layer. If you can't solve the XOR problem, you can't
solve MNIST.
- reuse memory. You are implementing iterative process so don't allocate new
vectors and matrices all the time. An immutable approach is nice but very
inappropriate. Also – don't copy data in some cache all the time, use indexes.
- objects are fine, but be careful about the depth of object hierarchy you are
going to create. Always remember that you are trying to be fast.
- double precision is fine. You may try to use floats. Do not use BigDecimals or
any other high precision objects.
- simple SGD is not strong enough, you are going to need to implement some
heuristics as well (or maybe not, but it's highly recommended). I suggest
heuristics: momentum, weight decay, dropout. If you are brave enough, you can
try RMSProp/AdaGrad/Adam.
- start with smaller networks and increase network topology carefully.
- play with hyperparameters.
- consider validation of the model using part of train dataset

### Even more tips from previous projects

- do not wait till the week before the deadline
- .exe files are not runnable on Aisa ... :)
    - Aisa runs on "Red Hat Enterprise Linux Server release 7.5 (Maipo)"
- missing or non-functional RUN script means that evaluation of your
implementation is not possible. So try to execute your RUN script on AISA
before your submission.
- if you are having a problem with missing compilers/maven on Aisa, you can
add such tools by including modules [3]. Please, do note, that if your
implementation requires such modules, your RUN script must include them as well,
otherwise, the RUN script won't work and I will have no clue what to include.
- do not shuffle testing data. It won't fit expected predictions.

### Hints

- for those of you working in a team, please, let me know in some text file
(README is fine) name and učo of team members.

 - some of you still struggling with implementation. Submit what you have even
if you can't reach 95% correct predictions under 30 minutes on AISA. I will give
you some notes regarding your implementation for resubmission.

- many of you have no idea what is happening during training. I suggest you to
pick some (fixed) random indexes of neurons and weights and print forward and
backward passes through the network (including inner potentials, activations,
weights values, gradients and error on the output). Understanding what is going
on can be very helpful during debugging.

- as I already stated, it is VERY difficult to find a minimal network for the
XOR task, so try more neurons in a hidden layer.